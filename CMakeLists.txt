cmake_minimum_required(VERSION 3.17)
project(image_rotate C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_FLAGS -std=c18 -pedantic-errors -Wall -Werror)

add_executable(image_rotate ../assignment-image-rotation/image_rotation/main.c ../assignment-image-rotation/image_rotation/image_io_processor.c ../assignment-image-rotation/image_rotation/image_io_processor.h ../assignment-image-rotation/image_rotation/image_bmp_io_processor.c ../assignment-image-rotation/image_rotation/image_bmp_io_processor.h ../assignment-image-rotation/image_rotation/statuses.h ../assignment-image-rotation/image_rotation/image_rotater.c ../assignment-image-rotation/image_rotation/image_rotater.h ../assignment-image-rotation/image_rotation/statuses.c ../assignment-image-rotation/image_rotation/util.h ../assignment-image-rotation/image_rotation/util.c)