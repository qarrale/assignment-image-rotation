//
// Created by qarrale on 30.01.2021.
//

#include "image_bmp_io_processor.h"


//http://www.ece.ualberta.ca/~elliott/ee552/studentAppNotes/2003_w/misc/bmp_file_format/bmp_file_format.htm

static bool is_bmp_header_correct(const struct bmp_header* header){

    if (header->bfType != BFTYPEFX) return false;
    if (header->bfReserved != RESERVED ) return false;
    if (header->biCompression != BI_RGB) return false;
    if (header->biSize != BMPSIZEHEADER) return false;
    if (header->biBitCount != BITPERPIXEL) return false;
    if (header->biPlanes != BMPPLANES) return false;

    return true;
}



static enum read_status get_bmp_header(FILE* in, struct bmp_header* header){

    if(fread(header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_INVALID_SIGNATURE;
    }
    if (!is_bmp_header_correct(header)){
        return READ_INVALID_HEADER;
    }
    if (fseek(in,header->bOffBits, 0)){
        return READ_INVALID_BITS;
    }
    return READ_OK;

}

static uint64_t get_padding(const uint32_t width){
    const uint64_t buffer = width*sizeof(struct pixel)%4;
    if ((width*sizeof(struct pixel)+buffer)%4 !=0 ){
        return 4-buffer;
    }
    return buffer;
}

static enum read_status get_bmp_body(FILE* in, struct image* bmp, struct bmp_header* bmp_header){

    for (size_t i = 0; i<bmp_header->biHeight; i++){

        fread(bmp->data+i*bmp_header->biWidth ,sizeof(struct pixel),(size_t)bmp_header->biWidth, in);

        if (ferror(in)) return READ_INVALID_BITS;

        fseek(in,get_padding(bmp_header->biWidth),1);
    }
    return READ_OK;
}


enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header= {0};

    const enum read_status get_bmp_header_result = get_bmp_header(in,&header);
    if (get_bmp_header_result != READ_OK) return get_bmp_header_result;

    *img = image_create(header.biWidth, header.biHeight);

    const enum read_status get_data_result = get_bmp_body(in,img,&header);
    if (get_data_result != READ_OK) return get_data_result;

    return READ_OK;
}


static struct bmp_header create_header( const uint32_t width, const uint32_t height ) {
    struct bmp_header header;

    header.bfType = BFTYPEFX;
    header.bfReserved = RESERVED;
    header.bOffBits =  sizeof( struct bmp_header );
    header.bfileSize = header.bOffBits + height * ( width * sizeof( struct pixel ) + get_padding(width) );
    header.biSize = BMPSIZEHEADER;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = BMPPLANES;
    header.biBitCount = BITPERPIXEL;
    header.biCompression = BI_RGB;
    header.biSizeImage = header.bfileSize;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum write_status to_bmp(FILE* out, struct image* img){

    struct bmp_header header = create_header(img->width,img->height);


    if (fwrite(&header, sizeof(struct bmp_header), 1, out)<1) return WRITE_ERROR;

    for (size_t i = 0; i < header.biHeight; i++) {
        if ( fwrite( img->data + i * header.biWidth, sizeof(struct pixel), header.biWidth, out ) < header.biWidth ) return WRITE_ERROR;
        if (fseek(out,get_padding(header.biWidth),1)) return WRITE_ERROR;
    }

    return WRITE_OK;

}


