//
// Created by qarrale on 30.01.2021.
//

#ifndef IMAGE_IMAGE_BMP_IO_PROCESSOR_H
#define IMAGE_IMAGE_BMP_IO_PROCESSOR_H
#include <stdio.h>
#include  <stdint.h>
#include <stdbool.h>
#include "statuses.h"
#include "image_rotater.h"

#define BFTYPEFX 0x4D42
//0 = BI_RGB no compression
#define BI_RGB 0
#define BMPSIZEHEADER 40
#define BMPPLANES 1
#define BITPERPIXEL 24
#define RESERVED 0


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)



enum write_status to_bmp(FILE* out, struct image* img);
enum read_status from_bmp( FILE* in, struct image* img );

#endif //IMAGE_IMAGE_BMP_IO_PROCESSOR_H
