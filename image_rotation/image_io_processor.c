//
// Created by qarrale on 30.01.2021.
//

#include "image_io_processor.h"

static struct image_format_processor format_mapper[]={
        [BMP] = {.format_name=".bmp",.reader=from_bmp,.writer=to_bmp}
};




enum read_status get_by_path( const char* input_path, struct image* const img ) {

    enum read_status read_status = READ_OK;
    FILE* input_file = fopen( input_path, "rb" );
    if ( input_file == NULL )  return READ_PATH_ERROR;

    if (strcmp(dot_split(input_path),format_mapper[BMP].format_name) == 0 ) {
         read_status=format_mapper[BMP].reader(input_file,img);
    }else {
        return READ_WRONG_FORMAT;
    }

    if ( fclose( input_file ) != 0 ){
        return READ_CLOSE_ERROR;
    }

    return read_status;
}

enum write_status set_by_path( const char* output_path, struct image * const img ) {

    enum write_status write_status = WRITE_OK;
    FILE* output_file = fopen( output_path, "wb" );
    if ( output_file == NULL )  return WRITE_FILE_DOESNT_EXIST;

    if (strcmp(dot_split(output_path),format_mapper[BMP].format_name) == 0 ) {
        write_status=format_mapper[BMP].writer(output_file,img);
    } else {
        return WRITE_WRONG_FORMAT;
    }

    if ( fclose( output_file ) != 0 ){
        return WRITE_ERROR;
    }

    return write_status;
}
