//
// Created by qarrale on 30.01.2021.
//


#ifndef IMAGE_IMAGE_IO_PROCESSOR_H
#define IMAGE_IMAGE_IO_PROCESSOR_H

#include  <stdint.h>
#include <stdio.h>
#include "image_bmp_io_processor.h"
#include <string.h>
#include <stdbool.h>
#include "statuses.h"
#include "util.h"

enum image_format{
    BMP=0,
    IMG
    /* другие форматы */
};


struct image_format_processor{
    const char* format_name;
    enum read_status (*reader)( FILE*, struct image*);
    enum write_status (*writer)( FILE*, struct image*);
};
enum read_status get_by_path( const char* input_path, struct image* const img );
enum write_status set_by_path( const char* output_path, struct image * const img );



#endif //IMAGE_IMAGE_IO_PROCESSOR_H
