//
// Created by qarrale on 31.01.2021.
//

#include "image_rotater.h"


struct image image_create (const int64_t w, const int64_t h){

    return (struct image){
        .height=h,
        .width=w,
        .data = malloc(w * h * sizeof(struct pixel))
    };
}

void image_destroy(struct image* img){
    free(img->data);
}

static uint64_t get_address(uint64_t x,uint64_t y,uint64_t width){
    return (y * width + x);
}

struct image image_rotate(struct image* img){


    struct image new_img  = image_create(img->height, img->width);

    for (uint64_t i=0; i<img->height; i++){
        for (uint64_t j=0; j<img->width; j++){
            new_img.data[get_address(img->height-1-i,j,img->height)] = img->data[get_address(j,i,img->width)];
        }
    }

    return new_img;
}