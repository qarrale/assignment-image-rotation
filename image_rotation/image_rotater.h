//
// Created by qarrale on 31.01.2021.
//

#ifndef IMAGE_ROTATE_IMAGE_ROTATER_H
#define IMAGE_ROTATE_IMAGE_ROTATER_H

#include <stdint.h>
#include <malloc.h>
#include <stdbool.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct pixel { uint8_t b, g, r; };

struct image image_create (int64_t w, int64_t h);
void image_destroy(struct image* img);
struct image image_rotate(struct image* img);

#endif //IMAGE_ROTATE_IMAGE_ROTATER_H
