
#include "image_io_processor.h"
#include "util.h"
#define SIGNATURE "You should use this command with next signature image_rotate [input_file] [output_file]\n "\
                  "if you want to apply rotation to the input file yo can use next signature image_rotate [input_file] \n"

int main( int argc, char** argv ) {

    char* in;
    char* out;
    struct image input = {0};

    if (argc<2 || argc > 3){
        err(SIGNATURE);
    }
    if (argc ==2 ){
        in = out =  argv[1];
    }
    if (argc ==3 ){
        in = argv[1];
        out = argv[2];
    }


    enum read_status input_status = get_by_path( in, &input );
    if (input_status != READ_OK) err(read_status_map[input_status]);

    struct image rotated = image_rotate(&input);

    enum write_status output_status = set_by_path( out, &rotated );
    if (output_status != WRITE_OK) err(write_status_map[output_status]);

    image_destroy(&input);
    image_destroy(&rotated);

    fprintf(stderr,"Your image was successfully rotated");
    return 0;
}

