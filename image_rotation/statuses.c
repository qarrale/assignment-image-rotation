//
// Created by qarrale on 31.01.2021.
//
#include "statuses.h"

const char* const read_status_map[] = {
        [READ_PATH_ERROR] = "Check input path",
        [READ_INVALID_BITS] = "This image was corrupted",
        [READ_INVALID_HEADER] = "Input file header is invalid",
        [READ_INVALID_SIGNATURE] = "Input file signature is invalid",
        [READ_OK]="Your input file was successfully read",
        [READ_WRONG_FORMAT] = "Wrong input file format",
        [READ_CLOSE_ERROR] = "Error closing file"
};

const char* const write_status_map[] = {
        [WRITE_ERROR] = "The process of writing wasn't finished due to output file was corrupted",
        [WRITE_WRONG_FORMAT] = "Output file format error",
        [WRITE_OK]="Output file was updated",
        [WRITE_FILE_DOESNT_EXIST] = "output file doesn't exist, check your path"
};

