//
// Created by qarrale on 31.01.2021.
//

#ifndef IMAGE_ROTATE_STATUSES_H
#define IMAGE_ROTATE_STATUSES_H

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_WRONG_FORMAT,
    READ_PATH_ERROR,
    READ_CLOSE_ERROR
};


enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FILE_DOESNT_EXIST,
    WRITE_WRONG_FORMAT,
    /* коды других ошибок  */
};

extern const char* const read_status_map[];
extern const char* const write_status_map[];


#endif //IMAGE_ROTATE_STATUSES_H
