//
// Created by qarrale on 04.02.2021.
//

#include "util.h"

__declspec(noreturn) void err( const char* msg, ... ) {
    va_list args;
    va_start (args, msg);
    fprintf(stderr, "err: ");
    vfprintf(stderr, msg, args);
    va_end (args);
    exit(1);
}
const char* dot_split(const char* path){
    size_t i= strlen(path)-1;
    char* dem = ".";
    while (path[i] != *dem){
        i--;
    }
    return &path[i];
}
