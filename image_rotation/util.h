//
// Created by qarrale on 03.02.2021.
//

#ifndef IMAGE_ROTATE_UTIL_H
#define IMAGE_ROTATE_UTIL_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* dot_split(const char* path);

__declspec(noreturn) void err( const char* msg, ... );

#endif //IMAGE_ROTATE_UTIL_H
